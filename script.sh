#! /usr/bin/env bash
echo "Installing apache2"
sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt-get install apache2 -y
sudo ufw allow 'Apache'

echo "Installing php"
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get install php7.1 libapache2-mod-php7.1 php7.1-common php7.1-mbstring php7.1-xmlrpc php7.1-soap php7.1-gd php7.1-xml php7.1-intl php7.1-mysql php7.1-cli php7.1-mcrypt php7.1-ldap php7.1-zip php7.1-curl -y

echo "Installing mysql"
sudo apt-get install mysql-server mysql-client -y
sudo mysql <<SQL_SC
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
CREATE DATABASE opencart;
CREATE USER 'opencartuser'@'localhost' IDENTIFIED BY 'opencart';
GRANT ALL ON opencart.* TO 'opencartuser'@'localhost' IDENTIFIED BY 'opencart' WITH GRANT OPTION;
FLUSH PRIVILEGES;
SQL_SC

sudo mv /var/www/project/upload /var/www/project/opencart
sudo cp /var/www/project/opencart/config-dist.php /var/www/project/opencart/config.php
sudo cp /var/www/project/opencart/admin/config-dist.php /var/www/project/opencart/admin/config.php
sudo chown -R www-data.www-data /var/www/project/opencart/
sudo chmod -R 755 /var/www/project/opencart/

sudo tee -a /etc/apache2/sites-available/opencart.conf > /dev/null <<EOT
<VirtualHost *:80>
     ServerAdmin admin@example.com
     DocumentRoot /var/www/project/opencart/
     ServerName myshop.com.test
     ServerAlias www.myshop.com.test

     <Directory /var/www/project/opencart/>
        Options FollowSymlinks
        AllowOverride All
        Order allow,deny
        allow from all
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/error.log
     CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOT

sudo a2dissite 000-default.conf
sudo a2ensite opencart.conf
sudo a2enmod rewrite
sudo a2enmod php7.1
sudo service apache2 restart
